package com.ikattey.policy.exception;

/**
 * Generic exception for errors encountered while managing policies.
 */
public class PolicyHandlingException extends Exception {

  public PolicyHandlingException(String message) {
    super(message);
  }
}
