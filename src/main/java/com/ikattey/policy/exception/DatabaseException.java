package com.ikattey.policy.exception;

/**
 * Exception thrown for DB issues
 */
public class DatabaseException extends Exception {

  public DatabaseException(String message) {
    super(message);
  }

  public DatabaseException(String message, Throwable cause) {
    super(message, cause);
  }
}
