package com.ikattey.policy.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown when a resource requested by API client request doesn't exist
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

  public NotFoundException(String message) {
    super(message);
  }
}
