package com.ikattey.policy.repository;

import com.ikattey.policy.model.Person;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {

  private static final Logger log = LoggerFactory.getLogger(PersonRepository.class);
  private static final String PERSON_INSERTION_SQL = "INSERT INTO person (first_name, second_name) values (?, ?)";
  private final JdbcTemplate jdbcTemplate;

  public PersonRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  /**
   * Inserts a new person in the database
   *
   * @param firstname  first name
   * @param secondName last name
   * @return a {@link Person} containing db generated id, firstname and secondName
   */
  public Optional<Long> createPerson(String firstname, String secondName) {
    KeyHolder keyHolder = new GeneratedKeyHolder();

    jdbcTemplate.update(connection -> {
      PreparedStatement ps = connection
          .prepareStatement(PERSON_INSERTION_SQL, Statement.RETURN_GENERATED_KEYS);
      ps.setString(1, firstname);
      ps.setString(2, secondName);
      return ps;
    }, keyHolder);

    if (keyHolder.getKey() == null) {
      log.warn("db id not retrieved for person firstname={}; secondName={}", firstname, secondName);
      return Optional.empty();
    }

    return Optional.of(keyHolder.getKey().longValue());
  }

  /**
   * Returns the ids that exist in the database, from the input list.
   * <p>
   * For example, if ids 1 and 2 exist in the database, getExistingIds(List.of(1,3,4) will return 1
   *
   * @param ids list of ids to check
   */
  public List<Long> getExistingIds(List<Long> ids) {
    String inSql = String.join(",", Collections.nCopies(ids.size(), "?"));
    return jdbcTemplate.query(
        String.format("SELECT id FROM person WHERE id IN (%s)", inSql),
        (rs, rowNum) -> rs.getLong("id"),
        ids.toArray());
  }
}
