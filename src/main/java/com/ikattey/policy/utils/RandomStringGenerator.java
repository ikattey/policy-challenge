package com.ikattey.policy.utils;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Generates Random strings.
 * <p>
 * Code adapted from <a href="https://www.baeldung.com/java-random-string">...</a>
 */
public class RandomStringGenerator {

  private static final int ASCII_CHAR_START = 48; // numeral 0
  private static final int ASCII_CHAR_END = 90; // uppercase Z

  private RandomStringGenerator() {
    // hide implicit public constructor
  }

  public static String generateAlphanum(int length) {
    return ThreadLocalRandom.current().ints(ASCII_CHAR_START, ASCII_CHAR_END + 1)
        // characters between 58 - 64 are symbols
        .filter(i -> (i <= 57 || i >= 65))
        .limit(length)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();
  }
}
