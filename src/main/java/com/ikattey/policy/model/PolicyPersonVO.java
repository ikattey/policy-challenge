package com.ikattey.policy.model;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * View object for retrieving Policy - Person mapping from database
 */
public class PolicyPersonVO {

  private long dbId;
  private String policyId;
  private java.sql.Date effectiveDate;
  private BigDecimal totalPremium;
  private long personDbId;
  private BigDecimal premium;
  private String firstName;
  private String secondName;

  public long getDbId() {
    return dbId;
  }

  public String getPolicyId() {
    return policyId;
  }

  public Date getEffectiveDate() {
    return effectiveDate;
  }

  public BigDecimal getTotalPremium() {
    return totalPremium;
  }

  public long getPersonDbId() {
    return personDbId;
  }

  public BigDecimal getPremium() {
    return premium;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  private PolicyPersonVO(long dbId, String policyId, Date effectiveDate, BigDecimal totalPremium,
      long personDbId, BigDecimal premium, String firstName, String secondName) {
    this.dbId = dbId;
    this.policyId = policyId;
    this.effectiveDate = effectiveDate;
    this.totalPremium = totalPremium;
    this.personDbId = personDbId;
    this.premium = premium;
    this.firstName = firstName;
    this.secondName = secondName;
  }

  public static class Builder {

    private long dbId;
    private String policyId;
    private Date effectiveDate;
    private BigDecimal totalPremium;
    private long personDbId;
    private BigDecimal premium;
    private String firstName;
    private String secondName;

    public Builder setDbId(long dbId) {
      this.dbId = dbId;
      return this;
    }

    public Builder setPolicyId(String policyId) {
      this.policyId = policyId;
      return this;
    }

    public Builder setEffectiveDate(Date effectiveDate) {
      this.effectiveDate = effectiveDate;
      return this;
    }

    public Builder setTotalPremium(BigDecimal totalPremium) {
      this.totalPremium = totalPremium;
      return this;
    }

    public Builder setPersonDbId(long personDbId) {
      this.personDbId = personDbId;
      return this;
    }

    public Builder setPremium(BigDecimal premium) {
      this.premium = premium;
      return this;
    }

    public Builder setFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public Builder setSecondName(String secondName) {
      this.secondName = secondName;
      return this;
    }

    public PolicyPersonVO build() {
      return new PolicyPersonVO(dbId, policyId, effectiveDate, totalPremium, personDbId, premium,
          firstName, secondName);
    }
  }
}
