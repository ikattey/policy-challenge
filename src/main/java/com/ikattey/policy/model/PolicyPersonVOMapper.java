package com.ikattey.policy.model;


import com.ikattey.policy.model.PolicyPersonVO.Builder;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Maps join query for retrieving policy and policy users into a {@link PolicyPersonVO} object.
 */
public class PolicyPersonVOMapper implements RowMapper<PolicyPersonVO> {


  @Override
  public PolicyPersonVO mapRow(ResultSet rs, int rowNum) throws SQLException {
    PolicyPersonVO.Builder builder = new Builder()
        .setDbId(rs.getInt("db_id"))
        .setPolicyId(rs.getString("policy_id"))
        .setEffectiveDate(rs.getDate("effective_date"))
        .setTotalPremium(rs.getBigDecimal("total_premium"))
        .setPremium(rs.getBigDecimal("premium"))
        .setPersonDbId(rs.getInt("person_id"))
        .setFirstName(rs.getString("first_name"))
        .setSecondName(rs.getString("second_name"));

    return builder.build();
  }
}
