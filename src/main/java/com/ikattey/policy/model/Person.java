package com.ikattey.policy.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
public class Person {

  /**
   * Use class instead of primitive so id can be null
   */
  private Long id;
  private String firstName;
  private String secondName;

  private BigDecimal premium;

  public Person() {
    // create explicit no-arg constructor
  }

  public Person(String firstName, String secondName, BigDecimal premium) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.premium = premium;
  }

  public Person(long id, String firstname, String secondName, BigDecimal premium) {
    this(firstname, secondName, premium);
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BigDecimal getPremium() {
    return premium;
  }

  @Override
  public String toString() {
    return "Person{" +
        "firstName='" + firstName + '\'' +
        ", secondName='" + secondName + '\'' +
        ", premium=" + premium +
        ", id=" + id +
        '}';
  }
}
