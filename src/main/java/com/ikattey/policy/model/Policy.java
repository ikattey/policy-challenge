package com.ikattey.policy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Policy {

  private String policyId;

  @JsonFormat(pattern = "dd.MM.yyyy")
  private LocalDate startDate;
  @JsonFormat(pattern = "dd.MM.yyyy")
  private LocalDate effectiveDate;

  @JsonFormat(pattern = "dd.MM.yyyy")
  private LocalDate requestDate;
  private List<Person> insuredPersons;

  private BigDecimal totalPremium;


  public String getPolicyId() {
    return policyId;
  }

  public void setPolicyId(String policyId) {
    this.policyId = policyId;
  }

  public LocalDate getEffectiveDate() {
    return effectiveDate;
  }

  public void setEffectiveDate(LocalDate effectiveDate) {
    this.effectiveDate = effectiveDate;
  }

  public List<Person> getInsuredPersons() {
    return insuredPersons;
  }

  public void setInsuredPersons(List<Person> insuredPersons) {
    this.insuredPersons = insuredPersons;
  }

  public BigDecimal getTotalPremium() {
    return totalPremium;
  }

  public void setTotalPremium(BigDecimal totalPremium) {
    this.totalPremium = totalPremium;
  }

  public LocalDate getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(LocalDate requestDate) {
    this.requestDate = requestDate;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }


  @Override
  public String toString() {
    return "Policy{" +
        "policyId='" + policyId + '\'' +
        ", startDate=" + startDate +
        ", effectiveDate=" + effectiveDate +
        ", requestDate=" + requestDate +
        ", insuredPersons=" + insuredPersons +
        ", totalPremium=" + totalPremium +
        '}';
  }
}
