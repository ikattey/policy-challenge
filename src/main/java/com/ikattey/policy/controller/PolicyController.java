package com.ikattey.policy.controller;

import com.ikattey.policy.exception.BadRequestException;
import com.ikattey.policy.exception.DatabaseException;
import com.ikattey.policy.exception.NotFoundException;
import com.ikattey.policy.exception.PolicyHandlingException;
import com.ikattey.policy.model.Person;
import com.ikattey.policy.model.Policy;
import com.ikattey.policy.service.PolicyService;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/policy")
public class PolicyController {

  private final PolicyService service;

  public PolicyController(PolicyService service) {
    this.service = service;
  }

  /**
   * Creates a new Policy
   */
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Policy> createPolicy(@RequestBody final Policy policyRequest)
      throws BadRequestException, DatabaseException, PolicyHandlingException {

    service.validateNewPolicy(policyRequest);
    List<Person> persons = service.createNewPerson(policyRequest.getInsuredPersons());

    Policy newPolicy = service.createNewPolicy(policyRequest.getStartDate(), persons);

    /*
     * API clients expect 'startDate' to be populated in policy creation response.
     * Copy value from 'effectiveDate' and set it to null (only required for serialization)
     */

    newPolicy.setStartDate(newPolicy.getEffectiveDate());
    newPolicy.setEffectiveDate(null);

    return ResponseEntity.accepted().body(newPolicy);
  }

  /**
   * Updates an existing policy
   */
  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Policy> modifyPolicy(@RequestBody final Policy policyRequest)
      throws BadRequestException, NotFoundException, DatabaseException {
    service.validatePolicyUpdate(policyRequest);

    // create new persons in modification request
    // this sets the id in Person objects
    List<Person> newPersons = policyRequest.getInsuredPersons().stream().filter(p ->
        p.getId() == null).collect(Collectors.toList());
    service.createNewPerson(newPersons);

    Policy modifiedPolicy = service.modifyPolicy(policyRequest.getEffectiveDate(),
        policyRequest.getInsuredPersons(),
        policyRequest.getPolicyId());

    return ResponseEntity.accepted().body(modifiedPolicy);
  }


  /**
   * Retrieves a single policy
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Policy> getPolicy(@RequestParam final String policyId,
      @RequestParam(required = false) @DateTimeFormat(pattern = "dd.MM.yyyy") final LocalDate requestDate)
      throws NotFoundException {
    // if requestDate isn't specified, fetch the policy state at the current date
    LocalDate date = requestDate != null ? requestDate : LocalDate.now();
    Policy policyState = service.getPolicyStateAtDate(policyId, date);

    /*
     * API clients expect 'requestDate' to be populated in response, and 'effectiveDate' to be omitted.
     */
    policyState.setRequestDate(date);
    policyState.setEffectiveDate(null);

    return ResponseEntity.ok(policyState);
  }
}
