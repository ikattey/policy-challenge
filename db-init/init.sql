CREATE TABLE IF NOT EXISTS person (
                                      id int NOT NULL AUTO_INCREMENT,
                                      first_name varchar(255) NOT NULL,
                                      second_name varchar(255) NOT NULL,
                                      PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS policy (
                                      id int NOT NULL AUTO_INCREMENT,
                                      policy_id varchar(10) NOT NULL,
                                      effective_date date NOT NULL,
                                      total_premium decimal(10,2) NOT NULL,
                                      PRIMARY KEY (id),
                                      INDEX(policy_id),
                                      INDEX (effective_date)
);

CREATE TABLE IF NOT EXISTS policy_persons (
                                              id int NOT NULL AUTO_INCREMENT,
                                              policy_id int NOT NULL,
                                              person_id int NOT NULL,
                                              premium decimal(10,2) NOT NULL,
                                              PRIMARY KEY (id),
                                              FOREIGN KEY (policy_id) REFERENCES policy(id),
                                              FOREIGN KEY (person_id) REFERENCES person(id)
);