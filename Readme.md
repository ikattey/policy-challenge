# Policy API Coding Challenge

This is a Spring Boot application providing a REST API for creating, modifying and retrieving
insurance policies.

## Requirements

For building and running the application you need:

- JDK 11+
- Docker

## Running the application locally

The recommended way to run this application is via Docker.

1. First build the application by running `./mvnw clean package` from the root directory.
2. Then run `docker-compose up` to start the API server and its database. 

The application runs on `localhost:8080` by default.

## Database
MySQL is used for persistence. The database initialisation script can be found at `./db-init/init.sql`.

## REST API
The REST API to the app is described below.

### Create a new policy
To create a new policy, issue the following request:

`POST /policy`

```
    curl -X POST  http://localhost:8080/policy -H 'Content-Type: application/json' \
        -d '{
                   "startDate": "16.10.2022",
                    "insuredPersons": [
                        {
                            "firstName": "Jane",
                            "secondName": "Johnson",
                            "premium": 12.90
                        },
                        {
                            "firstName": "Jack",
                            "secondName": "Doe",
                            "premium": 15.90
                        }
                    ]
                }'
```

### Modify a policy

To modify an existing policy, issue the following request, replacing `${policyId}` with a valid
policyId.

`PUT /policy`

```
    curl -X PUT  http://localhost:8080/policy -H 'Content-Type: application/json' \
    -data '{
	           "policyId":"${policyId}",
	           "effectiveDate": "22.10.2022",
	           "insuredPersons": [
		           {
			         "firstName": "Jane",
			         "secondName": "Johnson",
			         "premium": 990.90
		           }
	           ]
            }'
```

### Retrieve the state of a policy at a given date

Issue the following GET request to retrieve the state of a policy at a given date,
replacing `${policyId}` and `${requestDate}` with valid values. If the `requestDate` parameter is
omitted, the current date is used.

```
curl 'http://localhost:8080/policy?policyId=${policyId}&requestDate=${requestDate}'
```