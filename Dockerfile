FROM adoptopenjdk:11-jre-hotspot
COPY ./target/policy-app.jar application.jar
ENTRYPOINT ["java", "-jar", "application.jar"]